<div class="sidebar" data-image="{{ asset('frontend/assets/img/sidebar-5.jpg') }}">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#"class="simple-text">
                Stock-Book
            </a>
        </div>
        <ul class="nav">
            <li class="nav-item {{ request()->path() === '/' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('frontend.index') }}">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item {{ request()->path() === 'accounts' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('accounts.index') }}">
                    <i class="nc-icon nc-circle-09"></i>
                    <p>Bank Accounts</p>
                </a>
            </li>
            <li class="nav-item {{ request()->path() === 'stocks' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('stocks.index') }}">
                    <i class="nc-icon nc-paper-2"></i>
                    <p>Stocks</p>
                </a>
            </li>
            <li class="nav-item {{ request()->path() === 'demats' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('demats.all') }}">
                    <i class="nc-icon nc-credit-card"></i>
                    <p>Demats</p>
                </a>
            </li>

            <li class="nav-item {{ request()->path() === 'brokers' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('brokers.index') }}">
                    <i class="nc-icon nc-notes"></i>
                    <p>Brokers</p>
                </a>
            </li>
        </ul>
    </div>
</div>
