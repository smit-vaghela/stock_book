<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <button type="button" class="btn btn-outline-primary">View Orders</button>
            </div>
            <div class="col-md-6">
                <div class="operation-buttons pull-right">
                    <button type="button" class="btn btn-outline-success">Buy</button>
                    <button type="button" class="btn btn-outline-danger">Sell</button>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-center">


                    <div class="w-50 align-bottom">

                        <div class="card text-center ">
                            <div class="card-header">
                                <!-- Example split danger button -->
                                <div class="btn-group">

                                    <button type="button" class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Bank Account <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#"> Action</a>
                                    </div>
                                        <span>&nbsp &nbsp &nbsp</span>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Demat Account <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Next Action</a>
                                    </div>
                                        <span>&nbsp &nbsp &nbsp</span>
                                </div>
                            </div>
                            <div class="card-body">
                                <a href="#" class="btn btn-outline-primary">View Portfolio</a>
                            </div>
                        </div>

                </div>


        </div>
    </div>
</div>

