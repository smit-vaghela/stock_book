@extends('layouts.admin-panel.app')
@section('head-name',"Stocks")
@section('main-content')
<div class="content">
    <div class="container-fluid">

        <div class="card">
            <div class="card-header"><h2>Add New Stock</h2></div>
            <div class="card-body">
                <form action="{{ route('stocks.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="stock_name">Stock Name</label>
                        <input type="text"
                                class="form-control @error('stock_name') is-invalid @enderror"
                                id="stock_name"
                                value="{{ old('stock_name') }}"
                                placeholder="Enter Stock Name"
                                name="stock_name">
                            @error('stock_name')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label for="closing_price">Closing Price</label>
                        <input type="decimal"
                                class="form-control @error('closing_price') is-invalid @enderror"
                                id="closing_price"
                                value="{{ old('closing_price') }}"
                                placeholder="Enter Closing Price"
                                name="closing_price">
                            @error('closing_price')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                    </div>
                    <button type="submit" class="btn btn-outline-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection



