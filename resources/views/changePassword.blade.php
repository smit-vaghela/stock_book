@extends('layouts.admin-panel.app')
@section('head-name',"Change Password")

@section('main-content')
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header"><h2>Change Password</h2></div>
            <div class="card-body">
                <form action="{{ route('change.password') }}" method="POST">
                    @csrf
                    @foreach ($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                    @endforeach

                    <div class="form-group">
                        <label for="password">Current Password</label>
                        <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password" placeholder="Enter Current Password">
                    </div>

                    <div class="form-group">
                        <label for="password">New Password</label>
                        <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password" placeholder="Enter New Password">
                    </div>

                    <div class="form-group">
                        <label for="password">New Confirm Password</label>
                        <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password" placeholder="Confirm Password">
                    </div>
                    <button type="submit" class="btn btn-outline-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
