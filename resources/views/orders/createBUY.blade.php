@extends('layouts.admin-panel.app')
@section('head-name',"Orders")
@section('page-level-styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('main-content')
<div class="content">
    <div class="container-fluid">

        <div class="card">
            <div class="card-header"><h2>Buy Order</h2></div>
            <div class="card-body">
                <form action="{{ route('orders.store') }}" method="POST">
                    @csrf

                <input type="hidden" name="demat_id" value="{{ $demat_id }}">
                <input type="hidden" name="operation" value="buy">


                <div class="form-group">
                    <label for="stock_id">Stock</label>
                    <select name="stock_id" id="stock_id" class="form-control select2">
                        <option></option>
                        @foreach ($stocks as $stock)
                            @if($stock->id == old('stock_id'))
                                <option value="{{ $stock->id }}" selected>{{ $stock->stock_name }}</option>
                            @else
                                <option value="{{ $stock->id }}">{{ $stock->stock_name }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('stock_id')
                      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                    <div class="form-group">
                        <label for="dp_account_number">Price</label>
                        <input type="number"
                                step="0.01"
                                class="form-control @error('price') is-invalid @enderror"
                                id="price"
                                value="{{ old('price') }}"
                                placeholder="Enter price."
                                name="price"
                                onkeyup="calculateOrderCost()">
                            @error('price')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <input type="number"
                                class="form-control @error('quantity') is-invalid @enderror"
                                id="quantity"
                                value="{{ old('quantity') }}"
                                placeholder="Enter Quantity"
                                name="quantity"
                                onkeyup="calculateOrderCost()">
                            @error('quantity')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label for="cost">Total Order</label>
                        <input type="number"
                                class="form-control disabled"
                                id="cost"
                                value=""
                                >
                    </div>
                    <button type="submit" class="btn btn-outline-success">Buy Stock</button>
                    <a href="{{ route('frontent.portfolio.index',$demat_id) }}" class="btn btn-outline-primary">Back</a>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection
@section('page-level-scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    function calculateOrderCost()
    {
        price = $("#price").val();
        quantity = $("#quantity").val();
        cost = price * quantity;

        // console.log("Brokerage "+brokerage);
        // stt = Math.round((0.1/100 * cost) * 100) / 100;
        // console.log("STT "+stt);
        // trans = Math.round((0.00345/100 * cost) * 100) / 100;
        // console.log("Trans "+trans);
        // gst = Math.round((brokerage + trans) * (1.18) * 100) / 100;
        // console.log("gst "+gst);
        // sebi = Math.round((0.0001/100 * cost) * 100) / 100;
        // console.log("sebi "+sebi);
        // stampduty = Math.round((0.015/100 * cost) * 100) / 100;
        // console.log("stampduty "+stampduty);
        // cost = cost + brokerage + stt + trans + gst + sebi + stampduty;
        // cost = Math.round(cost * 100) / 100;
        $("#cost").attr("value",cost);
    }

    $('.select2').select2({
        placeholder: 'Select a Stock',
        allowClear: true
    });
</script>
@endsection


