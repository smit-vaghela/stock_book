@extends('layouts.admin-panel.app')
@section('head-name',"Orders")
@section('main-content')
<div class="content">
    <div class="container-fluid">
        {{-- @if ($orders->count() > 0) --}}
        <div class="d-flex justify-content-end mb-3">
            <a href="{{ route('frontent.portfolio.index',$orders[0]->demat->id) }}" class="btn btn-outline-primary">Back</a>
        </div>
        <div class="card">
            <div class="card-header"><h2>Orders</h2></div>
            <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Order Id</th>
                                <th scope="col">Dp Id</th>
                                <th scope="col">Stock</th>
                                <th scope="col">Date</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Operation</th>
                                <th scope="col">Order Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->demat->dp_id }}</td>
                                    <td>{{ $order->stock->stock_name }}</td>
                                    <td>{{ $order->created_at->format('d M, Y')}}</td>
                                    <td>{{ $order->price }}</td>
                                    <td>{{ $order->quantity }}</td>
                                    <td>{{ $order->operation }}</td>
                                    <td>{{ $order->price*$order->quantity }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

            </div>
        </div>
       {{-- @else
            <h5>Nothing to show</h5>
        @endif --}}

    </div>
</div>

<!-- DELETE MODAL -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Modal Title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST" id="deleteAccountForm">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    Are you sure, you want to delete this account?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-danger">Delete Account</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="mt-5">
    {{-- {{ $accounts->links('vendor.pagination.bootstrap-4') }} --}}
</div>
@endsection

@section('page-level-scripts')
    <script>
        function displayModal(accountId){
            var url = "/accounts/" + accountId;
            $("#deleteAccountForm").attr('action', url);
        }
    </script>
@endsection

