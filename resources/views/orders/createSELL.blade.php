@extends('layouts.admin-panel.app')
@section('head-name',"Orders")
@section('page-level-styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('main-content')
<div class="content">
    <div class="container-fluid">

        <div class="card">
            <div class="card-header"><h2>Sell Order</h2></div>
            <div class="card-body">
                <form action="{{ route('orders.store') }}" method="POST">
                    @csrf

                <input type="hidden" name="demat_id" value="{{ $demat_id }}">
                <input type="hidden" name="operation" value="sell">


                <h2>{{ $stock->stock_name }}</h2>
                <input type="hidden" name="stock_id" value="{{ $stock->id }}">

                    <div class="form-group">
                        <label for="dp_account_number">Price</label>
                        <input type="number"
                        step="0.01"
                                class="form-control @error('price') is-invalid @enderror"
                                id="price"
                                value="{{ old('price') }}"
                                placeholder="Enter price."
                                name="price"
                                onkeyup="calculateOrderCost()">
                            @error('price')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <input type="number"
                                class="form-control @error('quantity') is-invalid @enderror"
                                id="quantity"
                                value="{{ old('quantity') }}"
                                placeholder="Enter Quantity"
                                name="quantity"
                                onkeyup="calculateOrderCost()">
                            @error('quantity')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label for="cost">Total Order</label>
                        <input type="number"
                                class="form-control disabled"
                                id="cost"
                                value=""
                                >
                    </div>
                    <button type="submit" class="btn btn-outline-success">Sell Stock</button>
                    <a href="{{ route('frontent.portfolio.index',$demat_id) }}" class="btn btn-outline-primary">Back</a>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection
@section('page-level-scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        placeholder: 'Select a Stock',
        allowClear: true
    });

    function calculateOrderCost()
    {
        price = $("#price").val();
        quantity = $("#quantity").val();
        cost = price * quantity;
        $("#cost").attr("value",cost);
    }
</script>
@endsection


