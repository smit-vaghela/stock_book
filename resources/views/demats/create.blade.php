@extends('layouts.admin-panel.app')

@section('page-level-styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection


@section('main-content')
    <div class="card">
        <div class="card-header"><h2>Add New Demat</h2></div>
        <div class="card-body">
            <form action="{{ route('demats.store') }}" method="POST">
                @csrf

                <div class="form-group">
                    <label for="account_id">Account</label>
                    <select name="account_id" id="account_id" class="form-control select2">
                        <option></option>
                        @foreach ($accounts as $account)
                            @if($account->id == old('account_id'))
                                <option value="{{ $account->id }}" selected>{{ $account->bank_account_number }}</option>
                            @else
                                <option value="{{ $account->id }}">{{ $account->bank_account_number }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('account_id')
                      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="dp_id">Dp Id</label>
                    <input type="text"
                            class="form-control @error('dp_id') is-invalid @enderror"
                            id="dp_id"
                            value="{{ old('dp_id') }}"
                            placeholder="Enter Dp Id."
                            name="dp_id">
                        @error('dp_id')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="broker_id">Dp Name</label>
                    <select name="broker_id" id="broker_id" class="form-control select2">
                        <option></option>
                        @foreach ($brokers as $broker)
                            @if($broker->id == old('broker_id'))
                                <option value="{{ $broker->id }}" selected>{{ $broker->dp_name }}</option>
                            @else
                                <option value="{{ $broker->id }}">{{ $broker->dp_name }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('broker_id')
                      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="dp_account_number">Dp Account Number</label>
                    <input type="text"
                            class="form-control @error('dp_account_number') is-invalid @enderror"
                            id="dp_account_number"
                            value="{{ old('dp_account_number') }}"
                            placeholder="Enter Dp Account Number."
                            name="dp_account_number">
                        @error('dp_account_number')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="trading_account_number">Trading Account Number</label>
                    <input type="text"
                            class="form-control @error('trading_account_number') is-invalid @enderror"
                            id="trading_account_number"
                            value="{{ old('trading_account_number') }}"
                            placeholder="Enter Trading Account Number."
                            name="trading_account_number">
                        @error('trading_account_number')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>
                <form action="" method="GET" id="getDemats">
                    <input type="submit" class="btn btn-outline-primary" id="btn-demats" value="Submit">
                </form>
                {{-- <button type="submit" class="btn btn-outline-success">Submit</button> --}}
            </form>
        </div>
    </div>
@endsection
@section('page-level-scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        placeholder: 'Select an option',
        allowClear: true
    });

    flatpickr("#published_at", {
        enableTime: true,
        dateFormat: "Y-m-d H:i"
    });

    $('.select2Demats').select2({
        placeholder: 'Select a Bank Account',
        allowClear: true
    });

    $('#btn-demats').click(function(e){
        // e.preventDefault();
        selectedId = $("#account_id").children('option:selected').val();
        if(selectedId == 0)
        {
            return;
        }

        var url = "/account/" + selectedId + "/demats";
        $("#getDemats").attr('action',url)
    });
</script>
@endsection
