@extends('layouts.admin-panel.app')

@section('main-content')
    <div class="card">
        <div class="card-header"><h2>Edit Demat</h2></div>
        <div class="card-body">
            <form action="{{ route('demats.update', $demat->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="dp_id">Dp Id</label>
                    <input type="text"
                            class="form-control @error('dp_id') is-invalid @enderror"
                            id="dp_id"
                            value="{{ old('dp_id', $demat->dp_id) }}"
                            placeholder="Enter Dp Id."
                            name="dp_id">
                        @error('dp_id')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="broker_id">Dp Name</label>
                    <select name="broker_id" id="broker_id" class="form-control select2">
                        <option></option>
                        @foreach ($brokers as $broker)
                            @if($broker->id == old('broker_id', $demat->broker_id))
                                <option value="{{ $broker->id }}" selected>{{ $broker->dp_name }}</option>
                            @else
                                <option value="{{ $broker->id }}">{{ $broker->dp_name }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('broker_id')
                      <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="dp_account_number">Dp Account Number</label>
                    <input type="text"
                            class="form-control @error('dp_account_number') is-invalid @enderror"
                            id="dp_account_number"
                            value="{{ old('dp_account_number', $demat->dp_account_number ) }}"
                            placeholder="Enter Dp Account Number."
                            name="dp_account_number">
                        @error('dp_account_number')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="trading_account_number">Trading Account Number</label>
                    <input type="text"
                            class="form-control @error('trading_account_number') is-invalid @enderror"
                            id="trading_account_number"
                            value="{{ old('trading_account_number', $demat->trading_account_number) }}"
                            placeholder="Enter Trading Account Number."
                            name="trading_account_number">
                        @error('trading_account_number')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>
                    <input type="submit" class="btn btn-outline-primary" id="btn-demats" value="Submit">
            </form>

        </div>
    </div>
@endsection
@section('page-level-scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        placeholder: 'Select an option',
        allowClear: true
    });

    $('.select2Demats').select2({
        placeholder: 'Select a Bank Account',
        allowClear: true
    });
</script>
@endsection
@section('page-level-styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
