@extends('layouts.admin-panel.app')
@section('head-name',"Demats")
@section('main-content')
<div class="content">
<div class="container-fluid">
    <div class="d-flex justify-content-end mb-3">
        <a href="{{ route('demats.create') }}" class="btn btn-outline-primary">Add Demat</a>
    </div>
    <div class="card">
        <div class="card-header"><h2>Demat Accounts of {{ Auth::user()->name }}</h2></div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Bank Account Number</th>
                        <th scope="col">Bank Name</th>
                        <th scope="col">Dp Id</th>
                        <th scope="col">Dp Name</th>
                        <th scope="col">Dp Account Number</th>
                        <th scope="col">Trading Account Number </th>
                        <th scope="col">Actions </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($demats as $demat)
                        <tr>
                            <td>{{ $demat->account->bank_account_number }}</td>
                            <td>{{ $demat->account->bank_name }}</td>
                            <td>{{ $demat->dp_id }}</td>
                            <td>{{ $demat->broker->dp_name }}</td>
                            <td>{{ $demat->dp_account_number }}</td>
                            <td>{{ $demat->trading_account_number }}</td>
                            <td>
                                <a href="{{ route('demats.edit', $demat->id) }}" class="btn btn-sm btn-primary mr-2">Edit</a>
                                <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{ $demat->id }})" data-toggle="modal" data-target="#deleteModal">
                                    Delete
                                </button>
                                <a href="{{ route('frontent.portfolio.index', $demat->id) }}" class="btn btn-sm btn-success ml-2">View Portfolio</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
</div>
    <div class="mt-5">
        {{-- {{ $demats->links('vendor.pagination.bootstrap-4') }} --}}
    </div>
@endsection
