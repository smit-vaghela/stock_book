@extends('layouts.admin-panel.app')
@section('head-name',"Portfolio")
@section('page-level-styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<style>
    .portfolio-head{
        margin-bottom: 0;
        font-size: 2.5rem;
        font-weight: 200;
    }
</style>
@endsection
@section('main-content')
<div class="content mb-0">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('orders.index', $demat_id) }}" class="btn btn-outline-primary">View Orders</a>
                <a href="{{ route('orders.index.detailed', $demat_id) }}" class="btn btn-outline-primary">View Detailed Order List</a>
            </div>
            <div class="col-md-6">
                <div class="operation-buttons pull-right">
                    <a href="{{ route('orders.buy', $demat_id) }}" class="btn btn-outline-success">Buy Stock</a>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-6">
              <div class="card">
                <div class="card-body">
                    <p class="portfolio-head">{{ $stocks[1] }}</p>
                    <h4 class="card-title">Total Invested</h4>

                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card">
                <div class="card-body">
                    <p class="portfolio-head">{{ $stocks[2] }}</p>
                    <h4 class="card-title">Unrealized</h4>

                </div>
              </div>
            </div>
          </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">

                    <p class="portfolio-head">{{ $stocks[3] }}</p>
                    <h4 class="card-title">Current Market Value</h4>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">
                    <p class="portfolio-head">{{ $stocks[4]."%" }}</p>
                    <h4 class="card-title">P&L %</h4>

                  </div>
                </div>
              </div>
        </div>
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <h3>Current Holdings</h3>
                </div>
                <div class="card-body">

                    @if($stocks[0] != 0)
                    @foreach ($stocks[0] as $key => $value)
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-primary pull-left">{{ $key }}</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Quantity: <strong>{{ $value['quantity'] }}</strong></p>
                                    <p>Current Market Price : <strong>{{ $value['current_price'] }}</strong></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Without Taxes</h4>
                                        <hr>
                                    <p>Average Price: <strong>{{ $value['average_price'] }}</strong></p>
                                    <p>Total Value: <strong>{{ $value['value_at_cost'] }}</strong></p>
                                    <p>Value at Market: <strong>{{ $value['value_at_market'] }}</strong></p>
                                    <p>Unrealized P&L: <strong>{{ $value['unrealized'] }}</strong></p>
                                    <p>Unrealized P&L %: <strong>{{ $value['unrealized_per'] }}</strong></p>
                                </div>
                                <div class="col-md-6">
                                    <h5>With Taxes</h4>
                                        <hr>
                                    <p>Average Price: </p>
                                    <p>Total Value: <strong>{{ $value['value_with_tax'] }}</strong></p>
                                    <p>Value at Market:</p>
                                    <p>Unrealized P&L:</p>
                                    <p>Unrealized P&L %:</p>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('orders.sell', [$demat_id,$value['stock_id']]) }}" class="btn btn-danger pull-right">Sell</a>
                        </div>
                    </div>
                    @endforeach
                    @endif

                    <!--<table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Stock</th> {{-- Reliance --}}
                                <th scope="col">Average Price</th> {{-- 2000 --}}
                                <th scope="col">Current Market Price</th> {{-- 2500 --}}
                                <th scope="col">Qunatity</th> {{-- 10 --}}
                                <th scope="col">Value at Cost</th> {{-- 20000 --}}
                                <th scope="col">Value with Taxes</th> {{-- 20000 --}}
                                <th scope="col">Value at Market</th> {{-- 25000 --}}
                                <th scope="col">Unrealized P&L</th> {{-- 5000 --}}
                                <th scope="col">Unrealized P&L %</th> {{-- 25% --}}
                            </tr>
                        </thead>
                        <tbody>
                            @if($stocks[0] != 0)
                            @foreach ($stocks[0] as $key => $value)
                                <tr>
                                    <td class="text-center">{{ $loop->index+1; }}</td>
                                    <td class="text-center">{{ $key }}</td> {{-- Stock Name --}}
                                    <td class="text-center">{{ $value['average_price'] }}</td>{{-- Average Price --}}
                                    <td class="text-center">{{ $value['current_price'] }}</td>{{-- Current Market Price --}}
                                    <td class="text-center">{{ $value['quantity'] }}</td>{{-- Quantity --}}
                                    <td class="text-center">{{ $value['value_at_cost'] }}</td>{{-- Value at Cost --}}
                                    <td class="text-center">{{ $value['value_with_tax'] }}</td>{{-- Value with Taxes --}}
                                    <td class="text-center">{{ $value['value_at_market'] }}</td>{{-- Value at Market --}}
                                    <td class="text-center">{{ $value['unrealized'] }}</td>{{-- Unrealized P&L --}}
                                    <td class="text-center">{{ $value['unrealized_per']."%" }}</td>{{-- Unrealized P&L% --}}
                                    <td class="text-center"><a href="{{ route('orders.sell', [$demat_id,$value['stock_id']]) }}" class="btn btn-outline-danger">Sell</a></td>
                                </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>--!>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-level-scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        placeholder: 'Select a Demat Account',
        allowClear: true
    });
</script>
@endsection
