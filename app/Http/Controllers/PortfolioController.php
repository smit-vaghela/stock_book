<?php

namespace App\Http\Controllers;

use App\Models\Demat;
use App\Models\Order;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    protected $demat_id;
    public function index($id)
    {
        $this->demat_id = $id;
        $demat_id = $this->demat_id;
        $stocks = $this->getStocks($this->demat_id);
        return view('portfolio',compact(['demat_id','stocks']));
    }

    private function getStocks($id)
    {
        $orders = Order::with('stock')->getorderbydemat($id)->get();
        $stock_maintainer = [];
        foreach($orders as $order)
        {
            $stock_name = $order->stock->stock_name;
            if(isset($stock_maintainer[$stock_name]))// Is the Stock present in the list?
            {
                if($order->operation == 'buy')//What is the operation?
                {
                    array_push($stock_maintainer[$stock_name],[$order->quantity,$order->price,$order->stock,$order->demat->broker,$order->price_tax]);
                }
                else
                {
                    $stock_selling_quantity = $order->quantity;
                    while($stock_selling_quantity > 0)
                    {
                        $current_stock_quantity = $stock_maintainer[$stock_name][0][0];
                        $stock_maintainer[$stock_name][0][0] = $current_stock_quantity-$stock_selling_quantity;
                        if($stock_maintainer[$stock_name][0][0] <= 0)
                        {
                            array_splice($stock_maintainer[$stock_name], 0, 1);
                        }
                        $stock_selling_quantity = $stock_selling_quantity - $current_stock_quantity;
                    }
                }
            }
            else
            {
                $stock_maintainer[$stock_name] = array([$order->quantity,$order->price,$order->stock,$order->demat->broker,$order->price_tax]);
            }
        }
        return $this->calculateAvg($stock_maintainer);
    }

    private function calculateAvg($stocks)
    {

        $stocks_for_portfolio = [];
        foreach($stocks as $key => $value)
        {
            if($value == null)
            {
                continue;
            }
            $total_quantity = 0;
            $order_price = 0;
            $avg = [];
            foreach($value as $order)
            {
                $total_quantity = $total_quantity + $order[0];
                $order_price = ($order[1]*$order[0]);
                array_push($avg, $order_price);
            }
            $total_price = array_sum($avg);
            $total_avg = $total_price / $total_quantity;
            $stockObject = $value[0][2];
            $brokerObject = $value[0][3];
            $stocks_for_portfolio[$key] = [round($total_avg,2),$total_quantity,$stockObject,$brokerObject];
        }
        return $this->uiSetter($stocks_for_portfolio);
    }

    private function uiSetter($array_of_stocks)
    {
        // dd($array_of_stocks);
        if(count($array_of_stocks) == 0)
            return [0, 0, 0, 0, 0];
        $total_cost = 0;
        $total_unrealized = 0;
        $total_Pl_per = 0;
        $total_market_value = 0;
        $stocks = [];
        foreach($array_of_stocks as $key=>$value)
        {
            $total_cost = $total_cost + $value[0]*$value[1];
            $total_unrealized = $total_unrealized + ($value[1] * $value[2]->closing_price) - ($value[0]*$value[1]);
            $stocks[$key] = [
                'average_price' => $value[0], //Avg
                'current_price' => $value[2]->closing_price, // CMP
                'quantity' => $value[1], // quantity
                'value_at_cost' => $value[0]*$value[1], // AVG * Quantity
                'value_with_tax' => $value[0]*$value[1] + $value[0]*$value[1]*($value[3]->broker_charges/100),
                'value_at_market' => $value[1] * $value[2]->closing_price, // CMP * Quantity
                'unrealized' => ($value[1] * $value[2]->closing_price) - ($value[0]*$value[1]), // CMP * Quantity - AVG * Quantity
                'unrealized_per' => round((($value[2]->closing_price - $value[0])/$value[0])*100,2),// CMP * Quantity - AVG * Quantity %
                'stock_id'=>$value[2]->id
            ];
            $total_market_value = (($value[1] * $value[2]->closing_price) + $total_market_value);
            $total_Pl_per = round((($total_market_value / $total_cost) * 100) - 100, 2);
        }

        $demat = Demat::findOrFail($this->demat_id);

        $demat->total_invested = $total_cost;
        $demat->total_market_value = $total_market_value;

        $demat->save();

        return [$stocks, $total_cost, $total_unrealized, $total_market_value, $total_Pl_per];
    }

}
