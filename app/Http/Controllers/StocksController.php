<?php

namespace App\Http\Controllers;

use App\Http\Requests\Stocks\CreateStockRequest;
use App\Http\Requests\Stocks\UpdateStockRequest;
use App\Models\Stock;
use Illuminate\Http\Request;

class StocksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stocks = Stock::all();
        return view('stocks.index', compact('stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('stocks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStockRequest $request)
    {
        Stock::create([
            'stock_name' => $request->stock_name,
            'closing_price' => $request->closing_price
        ]);
        session()->flash('success', 'Stock Created Successfully!');
        return redirect(route('stocks.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        return view('stocks.edit', compact(['stock']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStockRequest $request, Stock $stock)
    {
        $stock->stock_name = $request->stock_name;
        $stock->closing_price = $request->closing_price;
        $stock->save();

        session()->flash('success','Stock updated successfully!');
        return redirect(route('stocks.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock = Stock::with('orders')->findorFail($id);
        // dd($stock->orders->count());
        if($stock->orders->count() != 0)
        {
            session()->flash('error', 'Stock cannot be Deleted!');
            return redirect(route('stocks.index'));
        }
        $stock->delete();
        session()->flash('success', 'Stock Deleted Successfully!');
        return redirect(route('stocks.index'));

        // if($stock->orders()->count() > 0){
        //     session()->flash('error','This Stock cannot be deleted!');
        //     return redirect(route('stocks.index'));
        // }
        // $stock->delete();


        // session()->flash('success','Stock deleted successfully!');
        // return redirect(route('stocks.index'));
    }
}
