<?php

namespace App\Http\Controllers;

use App\Exports\InvoicesExport;
use App\Http\Requests\Orders\CreateOrderRequest;
use App\Models\Demat;
use App\Models\Order;
use App\Models\Stock;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $orders = Order::with('stock','demat')->getorderbydemat($id)->get();
        return view('orders.index', compact(['orders']));
    }

    public function indexDetailed($id)
    {
        $orders = $this->getStocks($id);
        // $orders = $this->stackingOrders($orders);
        return view('orders.indexDetailed', compact(['orders']));
    }

    public function buyCreate($demat_id)
    {
        $stocks = Stock::all();
        return view('orders.createBUY', compact(['stocks','demat_id']));
    }

    public function sellCreate($demat_id,$stock_id)
    {
        $stock = Stock::findOrFail($stock_id);
        return view('orders.createSELL', compact(['stock','demat_id']));
    }

    public function store(CreateOrderRequest $request)
    {
        $broker = Demat::findOrFail($request->demat_id)->broker;
        if($request->operation == 'sell')
        {
            if($request->quantity > $this->checkStockQuantity($request->demat_id,$request->stock_id))
            {
                session()->flash('error',"Please check the Qunatity or the Stock Name you are trying to sell");
                return redirect(route('frontent.portfolio.index',$request->demat_id));
            }
            $price_tax = $this->calculateTax("sell",$request->price,$broker);
            $price_tax = $request->price - $price_tax; // Charged by broker
        }
        else
        {
            $price_tax = $this->calculateTax("buy",$request->price,$broker);
            $price_tax = $request->price + $price_tax; // Charged by broker
        }

        Order::create([
            'demat_id' => $request->demat_id,
            'stock_id' => $request->stock_id,
            'quantity' => $request->quantity,
            'operation' => $request->operation,
            'price' => $request->price,
            'price_tax' => $price_tax
        ]);
        session()->flash('success','Order created!');
        return redirect(route('frontent.portfolio.index',$request->demat_id));
    }


    public function destroy(Order $order)
    {
        //
    }

    private function checkStockQuantity($demat_id,$stock_id)
    {
        $total_quantity = 0;
        $orders = Order::where('stock_id',$stock_id)->getorderbydemat($demat_id)->get();
        foreach($orders as $order)
        {
            if($order->operation == 'buy')
            {
                $total_quantity = $total_quantity + $order->quantity;
            }
            else
            {
                $total_quantity = $total_quantity - $order->quantity;
            }
        }
        return $total_quantity;
    }


    private function getStocks($id)
    {
        $orders = Order::with('stock')->getorderbydemat($id)->get(); //gets all orders of the demat
        $stock_maintainer = [];

        foreach($orders as $order)
        {

            $stock_name = $order->stock->stock_name;
            if(isset($stock_maintainer[$stock_name]))// Is the Stock present in the list?
            {
                if($order->operation == 'buy')//What is the operation?
                {
                    array_push($stock_maintainer[$stock_name],[$order,"buy",$order->quantity]);
                }
                else
                {

                    $stock_selling_quantity = $order->quantity;
                    $order_counter = 0;
                    while($stock_selling_quantity > 0) //Loop till selling order quantity is 0
                    {
                        $current_order_quantity = $stock_maintainer[$stock_name][$order_counter][2];
                        if($current_order_quantity > 0)// Check on Buying orders' quantity
                        {
                            $setting_quantity = $current_order_quantity - $stock_selling_quantity;
                            if($setting_quantity <= 0)
                            {
                                $stock_maintainer[$stock_name][$order_counter][2] = 0;
                            }
                            else
                            {
                                $stock_maintainer[$stock_name][$order_counter][2] = $setting_quantity;
                            }
                            $stock_selling_quantity = $stock_selling_quantity - $current_order_quantity;
                        }
                        else
                        {
                            $order_counter++;
                        }

                    }
                    //Placing the Selling order in the array
                    $order_counter++;
                    if(isset($stock_maintainer[$stock_name][$order_counter+1]))//Check on the place after the last deducted buy order
                    {
                            while($stock_maintainer[$stock_name][$order_counter][1] != "buy")
                            {
                                $order_counter++;
                            }
                            $order_counter++;
                    }
                    else
                    {
                        $order_counter++;
                    }
                    array_splice( $stock_maintainer[$stock_name], $order_counter, 0, [[$order,"sell",$order->quantity]]);
                }
            }
            else
            {
                $stock_maintainer[$stock_name] = array([$order,"buy",$order->quantity]);
            }
        }
        return $stock_maintainer;
    }

    private function calculateTax($operation, $price, $broker)
    {
        $brokerage = round(($broker->broker_charges/100) * $price,2);
        $final_price = $brokerage;
        // $stt = round(0.1/100 * $price, 2);
        // $trans = round(0.00345/100 * $price,2);

        // $gst = round(($brokerage + $trans) * 0.18 ,2);

        // $sebi = round(0.0001/100 * $price, 2);

        // $stampduty = round(0.015/100 * $price,2);

        // if($operation == "buy")
        // {
        //     $final_price = $price + $brokerage + $stt + $trans + $gst + $sebi + $stampduty;

        // }
        // else if($operation == "sell")
        // {
        //     $final_price = $price - ($brokerage + $stt + $trans + $gst + $sebi + $stampduty);
        // }

        return $final_price;
    }

    public function export($id)
    {
        $orders = $this->getStocks($id);
        $orders = $this->stackingOrders($orders);
        return Excel::download(new InvoicesExport($orders), 'users.xlsx');
    }

    private function stackingOrders($orders)
    {
        $finalArray = [];
        array_push($finalArray, [
            'Order ID',
            'Stock Name',
            'Date Bought',
            'Price',
            'Quantity',
            'Operation',
            'Total Value',
            'Order ID',
            'Date Bought',
            'Price',
            'Quantity',
            'Operation',
            'Total Value'
        ]);
        $newArr = array();

        foreach ($orders as $key => $values)
        {
            foreach($values as $value)
                {array_push($newArr, $value);}
        }

        $orders = $newArr;
        // dd($orders);

            $putArray = [];
        for($x=0; $x<count($orders); $x++)
        {
            $currentOrderObject = $orders[$x][0];

            if($orders[$x][1] == "buy")
            {
                $putArray = [];
                array_push($putArray,
                           $currentOrderObject->id,
                           $currentOrderObject->stock->stock_name,
                           $currentOrderObject->created_at->format('d-m-Y'),
                           $currentOrderObject->price,
                           $currentOrderObject->quantity,
                           $currentOrderObject->operation,
                           $currentOrderObject->price * $currentOrderObject->quantity,
                          );
                continue;
            }
            else
            {
                if($orders[$x-1][1] == "buy")
                {
                    array_push($putArray,
                           $currentOrderObject->id,
                           $currentOrderObject->created_at->format('d-m-Y'),
                           $currentOrderObject->price,
                           $currentOrderObject->quantity,
                           $currentOrderObject->operation,
                           $currentOrderObject->price * $currentOrderObject->quantity,
                          );
                }
                else
                {
                    $putArray = [];
                    array_push($putArray,' ',' ',' ',' ',' ',' ',' ',
                           $currentOrderObject->id,
                           $currentOrderObject->created_at->format('d-m-Y'),
                           $currentOrderObject->price,
                           $currentOrderObject->quantity,
                           $currentOrderObject->operation,
                           $currentOrderObject->price * $currentOrderObject->quantity,
                          );

                }
                array_push($finalArray, $putArray);
            }

        }

        if(isset($putArray))
        {
            array_push($finalArray, $putArray);
        }

        return $finalArray;

    }
}
