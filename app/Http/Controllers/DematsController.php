<?php

namespace App\Http\Controllers;

use App\Http\Requests\Demats\CreateDematRequest;
use App\Http\Requests\Demats\UpdateDematRequest;
use App\Models\Account;
use App\Models\Broker;
use App\Models\Demat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DematsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $demats = Demat::all();
        return view('demats.index', compact('demats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accounts = Account::where('user_id',Auth::user()->id)->get();
        $brokers = Broker::all();
        return view('demats.create', compact(['accounts','brokers']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDematRequest $request)
    {
        $account = Account::findOrFail($request->account_id);
        Demat::create([
            'account_id' => $request->account_id,
            'dp_id' => $request->dp_id,
            'broker_id' => $request->broker_id,
            'dp_account_number' => $request->dp_account_number,
            'trading_account_number' => $request->trading_account_number,
            'total_invested' => 0,
            'total_market_value' => 0
        ]);
        session()->flash('success', 'Demat Created Successfully!');
        $demats = $account->demats;
        return view('demats.index', compact('demats'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Demat  $demat
     * @return \Illuminate\Http\Response
     */
    public function show(Demat $demat)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Demat  $demat
     * @return \Illuminate\Http\Response
     */
    public function edit(Demat $demat)
    {
        $brokers = Broker::all();
        return view('demats.edit', compact(['demat', 'brokers']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Demat  $demat
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDematRequest $request, Demat $demat)
    {
        $demat->dp_id = $request->dp_id;
        $demat->broker_id = $request->broker_id;
        $demat->dp_account_number = $request->dp_account_number;
        $demat->trading_account_number = $request->trading_account_number;

        $demat->save();

        session()->flash('success','Demat updated successfully!');
        return redirect(route('account.demats', $demat->account_id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Demat  $demat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Demat $demat)
    {
        if($demat->orders()->count() > 0){
            session()->flash('error','This Demat cannot be deleted as it has stocks!');
            return redirect(route('demats.index'));
        }
        $demat->delete();


        session()->flash('success','Demat deleted successfully!');
        return redirect(route('demats.index'));
    }

    public function getDematsForAccount(Request $request, Account $account)
    {
        $demats = $account->demats;
        return view('demats.index', compact('demats'));
    }

    public function displayAllDematsOfUser()
    {
        $demats = Demat::whereHas('account', function($query){
            $query->where('user_id', auth()->user()->id);
        })->get();
        return view('demats.viewAllDemats', compact('demats'));
    }
}
