<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function demat()
    {
        return $this->belongsTo(Demat::class);
    }

    public function stock()
    {
        return $this->belongsTo(Stock::class);
    }

    public function scopeGetorderbydemat($query,$id)
    {
        return $query->where('demat_id',$id);
    }


    //Mutators
    public function setPriceAttribute(string $price)
    {
        $this->attributes['price'] = $price;
        $broker = Demat::findOrFail($this->demat_id)->broker;


    }
}
