<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Demat extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    public function broker()
    {
        return $this->belongsTo(Broker::class);
    }
    //Scopes
    public function scopeGetdematsbyaccount($query)
    {
        $accounts_id = Account::where('user_id',Auth::user()->id)->pluck('id')->toArray();
        $demats = $query->whereIn('account_id',$accounts_id);
        return  $demats;
    }
}
