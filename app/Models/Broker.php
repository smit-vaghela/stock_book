<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Broker extends Model
{
    use HasFactory;
    protected $fillable = ['dp_name', 'broker_charges'];

    public function demats()
    {
        return $this->hasMany(Demat::class);
    }
}
