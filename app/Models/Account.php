<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Account extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'pan_number', 'aadhar_number', 'bank_name', 'bank_account_number'];

    public function demats()
    {
        return $this->hasMany(Demat::class);
    }

    //Scopes
    public function scopeGetbankaccountnumberbyuser($query)
    {
        $accounts = Account::where('user_id',Auth::user()->id)->get();
        return  $accounts;
    }
}
