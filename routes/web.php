<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccountsController;
use App\Http\Controllers\BrokersController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\DematsController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\StocksController;
use App\Http\Controllers\UsersController;

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->group(function() {

    Route::get('/', [FrontendController::class, 'index'])->name('frontend.index');
    Route::get('/portfolio/{demat}', [PortfolioController::class, 'index'])->name('frontent.portfolio.index');
    Route::get('/account/{account}/demats', [DematsController::class, 'getDematsForAccount'])->name('account.demats');
    Route::get('orders/{demat_id}', [OrdersController::class, 'index'])->name('orders.index');
    Route::get('orders/{demat_id}/buy', [OrdersController::class, 'buyCreate'])->name('orders.buy');
    Route::get('orders/{demat_id}/sell/{stock_id}', [OrdersController::class, 'sellCreate'])->name('orders.sell');
    Route::post('orders/', [OrdersController::class, 'store'])->name('orders.store');
    Route::get('demats/all', [DematsController::class, 'displayAllDematsOfUser'])->name('demats.all');
    Route::get('change-password', [ChangePasswordController::class, 'index'])->name('changePassword.index');
    Route::post('change-password', [ChangePasswordController::class, 'store'])->name('change.password');
    Route::get('orders/{demat_id}/detailed', [OrdersController::class, 'indexDetailed'])->name('orders.index.detailed');
    Route::get('export/{demat_id}', [OrdersController::class, 'export'])->name('export');



    Route::resource('accounts', AccountsController::class);
    Route::resource('demats', DematsController::class);
    Route::resource('stocks', StocksController::class);
    // Route::resource('orders', OrdersController::class);
    Route::resource('users', UsersController::class);
    Route::resource('brokers', BrokersController::class);
});
