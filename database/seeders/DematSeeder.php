<?php

namespace Database\Seeders;

use App\Models\Demat;
use Illuminate\Database\Seeder;

class DematSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Demat::create([
            'dp_id' => 'IN123457',
            // 'dp_name' => 'HDFC Bank',
            'dp_account_number' => '1234457891',
            'trading_account_number' => '1234567811',
            'account_id' => '1'
        ]);

        Demat::create([
            'dp_id' => 'IN123458',
            // 'dp_name' => 'Yes Bank',
            'dp_account_number' => '1234567846',
            'trading_account_number' => '1234567812',
            'account_id' => '2'
        ]);

        Demat::create([
            'dp_id' => 'IN123459',
            // 'dp_name' => 'Axis Bank',
            'dp_account_number' => '1234567847',
            'trading_account_number' => '1234567813',
            'account_id' => '3'
        ]);

        Demat::create([
            'dp_id' => 'IN123460',
            // 'dp_name' => 'ICICI Bank',
            'dp_account_number' => '1234567848',
            'trading_account_number' => '1234567814',
            'account_id' => '4'
        ]);

        Demat::create([
            'dp_id' => 'IN123461',
            // 'dp_name' => 'State Bank Of India',
            'dp_account_number' => '1234567849',
            'trading_account_number' => '1234567815',
            'account_id' => '5'
        ]);

        Demat::create([
            'dp_id' => 'IN123462',
            // 'dp_name' => 'HDFC Bank',
            'dp_account_number' => '1234567850',
            'trading_account_number' => '1234567816',
            'account_id' => '6'
        ]);

        Demat::create([
            'dp_id' => 'IN123463',
            // 'dp_name' => 'Yes Bank',
            'dp_account_number' => '1234567851',
            'trading_account_number' => '1234567817',
            'account_id' => '7'
        ]);

        Demat::create([
            'dp_id' => 'IN123464',
            // 'dp_name' => 'State Bank Of India',
            'dp_account_number' => '1234567852',
            'trading_account_number' => '1234567818',
            'account_id' => '8'
        ]);

        Demat::create([
            'dp_id' => 'IN123465',
            // 'dp_name' => 'ICICI Bank',
            'dp_account_number' => '1234567853',
            'trading_account_number' => '1234567819',
            'account_id' => '9'
        ]);

        Demat::create([
            'dp_id' => 'IN123466',
            // 'dp_name' => 'Axis Bank',
            'dp_account_number' => '1234567854',
            'trading_account_number' => '1234567810',
            'account_id' => '10'
        ]);

        Demat::create([
            'dp_id' => 'IN123467',
            // 'dp_name' => 'Yes Bank',
            'dp_account_number' => '1234567855',
            'trading_account_number' => '1234567811',
            'account_id' => '11'
        ]);

        Demat::create([
            'dp_id' => 'IN123468',
            // 'dp_name' => 'HDFC Bank',
            'dp_account_number' => '1234567856',
            'trading_account_number' => '1234567812',
            'account_id' => '12'
        ]);

        Demat::create([
            'dp_id' => 'IN123469',
            // 'dp_name' => 'HDFC Bank',
            'dp_account_number' => '1234567857',
            'trading_account_number' => '1234567813',
            'account_id' => '6'
        ]);

        Demat::create([
            'dp_id' => 'IN123470',
            // 'dp_name' => 'ICICI Bank',
            'dp_account_number' => '1234567858',
            'trading_account_number' => '1234567814',
            'account_id' => '9'
        ]);

        Demat::create([
            'dp_id' => 'IN123471',
            // 'dp_name' => 'Yes Bank',
            'dp_account_number' => '1234567859',
            'trading_account_number' => '1234567815',
            'account_id' => '2'
        ]);

        Demat::create([
            'dp_id' => 'IN123472',
            // 'dp_name' => 'HDFC Bank',
            'dp_account_number' => '1234567860',
            'trading_account_number' => '1234567816',
            'account_id' => '12'
        ]);

        Demat::create([
            'dp_id' => 'IN123473',
            // 'dp_name' => 'ICICI Bank',
            'dp_account_number' => '1234567861',
            'trading_account_number' => '1234567817',
            'account_id' => '4'
        ]);

        Demat::create([
            'dp_id' => 'IN123474',
            // 'dp_name' => 'Axis Bank',
            'dp_account_number' => '1234567862',
            'trading_account_number' => '1234567818',
            'account_id' => '10'
        ]);

        Demat::create([
            'dp_id' => 'IN123475',
            // 'dp_name' => 'State Bank Of India',
            'dp_account_number' => '1234567863',
            'trading_account_number' => '1234567819',
            'account_id' => '5'
        ]);

        Demat::create([
            'dp_id' => 'IN123476',
            // 'dp_name' => 'Yes Bank',
            'dp_account_number' => '1234567864',
            'trading_account_number' => '1234567820',
            'account_id' => '7'
        ]);

    }
}
