<?php

namespace Database\Seeders;

use App\Models\Order;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    //     Order::create([
    //         'price' => 52,
    //         'quantity' => 20,
    //         'operation' => 'sell',
    //         'demat_id' => 1,
    //         'stock_id' => 1
    //     ]);

    //     Order::create([
    //         'price' => 34,
    //         'quantity' => 60,
    //         'operation' => 'buy',
    //         'demat_id' => 1,
    //         'stock_id' => 3
    //     ]);

    //     Order::create([
    //         'price' => 520,
    //         'quantity' => 10,
    //         'operation' => 'sell',
    //         'demat_id' => 2,
    //         'stock_id' => 5
    //     ]);

    //     Order::create([
    //         'price' => 34,
    //         'quantity' => 20,
    //         'operation' => 'sell',
    //         'demat_id' => 8,
    //         'stock_id' => 11
    //     ]);

    //     Order::create([
    //         'price' => 222,
    //         'quantity' => 5,
    //         'operation' => 'buy',
    //         'demat_id' => 13,
    //         'stock_id' => 6
    //     ]);

    //     Order::create([
    //         'price' => 124,
    //         'quantity' => 10,
    //         'operation' => 'buy',
    //         'demat_id' => 12,
    //         'stock_id' => 2
    //     ]);

    //     Order::create([
    //         'price' => 34,
    //         'quantity' => 5,
    //         'operation' => 'buy',
    //         'demat_id' => 4,
    //         'stock_id' => 15
    //     ]);

    //     Order::create([
    //         'price' => 12,
    //         'quantity' => 34,
    //         'operation' => 'sell',
    //         'demat_id' => 12,
    //         'stock_id' => 7
    //     ]);

    //     Order::create([
    //         'price' => 45,
    //         'quantity' => 76,
    //         'operation' => 'sell',
    //         'demat_id' => 16,
    //         'stock_id' => 2
    //     ]);

    //     Order::create([
    //         'price' => 23,
    //         'quantity' => 12,
    //         'operation' => 'buy',
    //         'demat_id' => 3,
    //         'stock_id' => 14
    //     ]);

    //     Order::create([
    //         'price' => 48,
    //         'quantity' => 10,
    //         'operation' => 'buy',
    //         'demat_id' => 3,
    //         'stock_id' => 7
    //     ]);

    //     Order::create([
    //         'price' => 5223,
    //         'quantity' => 10,
    //         'operation' => 'sell',
    //         'demat_id' => 2,
    //         'stock_id' => 5
    //     ]);

    //     Order::create([
    //         'price' => 12,
    //         'quantity' => 45,
    //         'operation' => 'sell',
    //         'demat_id' => 14,
    //         'stock_id' => 4
    //     ]);

    //     Order::create([
    //         'price' => 45,
    //         'quantity' => 30,
    //         'operation' => 'buy',
    //         'demat_id' => 5,
    //         'stock_id' => 15
    //     ]);

    //     Order::create([
    //         'price' => 452,
    //         'quantity' => 80,
    //         'operation' => 'sell',
    //         'demat_id' => 19,
    //         'stock_id' => 9
    //     ]);

    //     Order::create([
    //         'price' => 87,
    //         'quantity' => 12,
    //         'operation' => 'buy',
    //         'demat_id' => 3,
    //         'stock_id' => 1
    //     ]);

    //     Order::create([
    //         'price' => 12,
    //         'quantity' => 230,
    //         'operation' => 'sell',
    //         'demat_id' => 11,
    //         'stock_id' => 4
    //     ]);

    //     Order::create([
    //         'price' => 556,
    //         'quantity' => 210,
    //         'operation' => 'buy',
    //         'demat_id' => 5,
    //         'stock_id' => 13
    //     ]);

    //     Order::create([
    //         'price' => 545,
    //         'quantity' => 10,
    //         'operation' => 'sell',
    //         'demat_id' => 7,
    //         'stock_id' => 13
    //     ]);

    //     Order::create([
    //         'price' => 54,
    //         'quantity' => 10,
    //         'operation' => 'buy',
    //         'demat_id' => 10,
    //         'stock_id' => 1
    //     ]);
    }
}
