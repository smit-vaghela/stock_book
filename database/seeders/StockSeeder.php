<?php

namespace Database\Seeders;

use App\Models\Stock;
use Illuminate\Database\Seeder;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Stock::create([
            'stock_name' => 'Subex',
            'closing_price' => 55.4
        ]);

        Stock::create([
            'stock_name' => 'Yes Bank',
            'closing_price' => 345.1
        ]);
        Stock::create([
            'stock_name' => 'HFCl',
            'closing_price' => 808
        ]);

        Stock::create([
            'stock_name' => 'IOC',
            'closing_price' => 130
        ]);

        Stock::create([
            'stock_name' => 'PNB',
            'closing_price' => 5604
        ]);

        Stock::create([
            'stock_name' => 'Adani Power',
            'closing_price' => 23.2
        ]);

        Stock::create([
            'stock_name' => 'Tata Power',
            'closing_price' => 45.6
        ]);

        Stock::create([
            'stock_name' => 'Nalco',
            'closing_price' => 87.1
        ]);

        Stock::create([
            'stock_name' => 'ACC',
            'closing_price' => 34.5
        ]);

        Stock::create([
            'stock_name' => 'Ultra Cement',
            'closing_price' => 67.1
        ]);

        Stock::create([
            'stock_name' => 'Mirc Electronics',
            'closing_price' => 98.5
        ]);

        Stock::create([
            'stock_name' => 'UCO Bank',
            'closing_price' => 101.6
        ]);

        Stock::create([
            'stock_name' => 'Pennar Industries',
            'closing_price' => 556.1
        ]);

        Stock::create([
            'stock_name' => 'Happiest Minds',
            'closing_price' => 76.5
        ]);

        Stock::create([
            'stock_name' => 'Tata Chemical',
            'closing_price' => 89.3
        ]);
    }
}
