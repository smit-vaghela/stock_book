<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'phone_number' => 9820098200,
            'password' => Hash::make('admin')
        ]);

        User::create([
            'name' => 'Himanshu Thakkar',
            'email' => 'himanshuthakkar@studylinkclasses.com',
            'phone_number' => 8879090961,
            'password' => Hash::make('abcd1234')
        ]);

        User::create([
            'name' => 'Yash Shirish Mehta',
            'email' => 'yashmehta2711@gmail.com',
            'phone_number' => 9820293336,
            'password' => Hash::make('abcd1234')
        ]);

        User::create([
            'name' => 'Prem Dinesh Mirani',
            'email' => 'premmirani9@gmail.com',
            'phone_number' => 9004425292,
            'password' => Hash::make('abcd1234')
        ]);

        User::create([
            'name' => 'Smit Rajesh Vaghela',
            'email' => 'smitvaghela33@gmail.com',
            'phone_number' => 9867225588,
            'password' => Hash::make('abcd1234')
        ]);

        User::create([
            'name' => 'Manav Ritesh Shah',
            'email' => 'manavshah28@gmail.com',
            'phone_number' => 7506178791,
            'password' => Hash::make('abcd1234')
        ]);
    }
}
